
function returnEndDate(date){ //function returns end date of ccurrent showing month
    return new Date(date.getFullYear(),date.getMonth()+1,0).getDate();
    }
    
    function colorSelectDate(){
        var d= document.getElementById("maxdate").value;
        var cel=document.getElementById(d);
        cel.classList.toggle("green");
    }
    
    function render(date){
    
       var months=["January","February","March","April","May","June","July","August","September","October","November","December"];
    
        var month=months[date.getMonth()];
        var year=date.getFullYear();
        var sday=new Date(date.getFullYear(),date.getMonth(),1).getDay();
        var enddate= returnEndDate(date);
       
        var maxdate=document.getElementById("maxdate");
        maxdate.setAttribute("max",enddate);
       document.getElementById("month").innerHTML=month;
       document.getElementById("date_str").innerHTML=year
    
       var cells="";
    
     for(x=sday;x>0;x--){
        cells+="<div>" + " " + "</div>";
    }
    for( i=1;i<=enddate;i++){
        cells+= "<div id=\"" + i + "\">" + i + "</div>";
    }
    document.getElementsByClassName("days")[0].innerHTML= cells;
    }
    
    function showCal(){
        var smon=document.getElementById("mon").value;
        var syea=document.getElementById("yea").value;
       // console.log(parseInt(smon)+parseInt(syea));//Testing Purpose
        var sdat=new Date(parseInt(syea),parseInt(smon),1);
        render(sdat);
    }
    
    var dt= new Date();
    render(dt);
    
    document.getElementById("btn").addEventListener("click",showCal);
    document.getElementById("colorChangeDate").addEventListener("click",colorSelectDate);
    
    
    
    