let hr=document.getElementById("hour");
let min=document.getElementById("min");
let sec=document.getElementById("sec");

function displayTime(){
    let date = new Date(); //Date object created

    //Getting hour, mins, secs from date object
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();

    let hRotation = 30*hh + mm/2;  //Hour hand rotation angle
    let mRotation = 6*mm; //Minutes hand rotation angle
    let sRotation = 6*ss; //Seconds hand rotation angle

    hr.style.transform = `rotate(${hRotation}deg)`;  // adding style to hour hand 
    min.style.transform = `rotate(${mRotation}deg)`; //adding style to min hand
    sec.style.transform = `rotate(${sRotation}deg)`; //adding style to second hand

}

setInterval(displayTime,1000); // functin to execute every second to update Hour, Min, Sec hands